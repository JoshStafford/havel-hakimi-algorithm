#include <vector>
#include <iostream>
#include "vector.h"

bool havel_hakimi(Vector v) {

	while (true) {

		// Step 1
		v.elim_zero();
		if (v.data.size() == 0) {
			std::cout << "Possible" << std::endl;
			return true;
		}

		// Step 2
		v.desc_sort();

		// Step 3
		int n = v.pop();

		// Step 4
		if (v.length_check(n)) {
			std::cout << "Not Possible" << std::endl;
			return false;
		}

		v.front_elim(n);

	}


}

int main() {

	Vector v = Vector({ 16, 9, 9, 15, 9, 7, 9, 11, 17, 11, 4, 9, 12, 14, 14, 12, 17, 0, 3, 16 });

	bool ans = havel_hakimi(v);

	std::cout << "Answer returned: " << ans << std::endl;


}

