#pragma once
#include <vector>


class Vector {

	public:
		std::vector<int> data;

	Vector(std::vector<int> _data){
		data = _data;
	}

	void elim_zero() {
		std::vector<int> result;

		for (int i = 0; i < data.size(); i++) {
			if (data[i] != 0) {
				result.push_back(data[i]);
			}
		}

		data = result;
	}

	void desc_sort() {
		bool searching = true;
		int comparisons;

		while (searching) {
			comparisons = 0;
			for (int i = 0; i < data.size() - 1; i++) {
				if (data[i] < data[i + 1]) {
					int temp = data[i + 1];
					data[i + 1] = data[i];
					data[i] = temp;

					comparisons++;
				}
			}
			if (comparisons == 0) {
				searching = false;
			}
		}

	}

	inline bool length_check(int len) const {
		if (len > data.size()) { return true; }
		else { return false; }
	}

	void front_elim(int num) {
		for (int i = 0; i < num; i++) {
			data[i]--;
		}
	}

	void show() {
		for (int i = 0; i < data.size(); i++) {
			std::cout << data[i] << ", ";
		}
		std::cout << std::endl;
	}

	int pop() {
		std::vector<int> result;

		for (int i = 1; i < data.size(); i++) {
			result.push_back(data[i]);
		}

		int val = data[0];
		data = result;
		return val;
	}

};