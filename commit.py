import os
from time import sleep
from sys import argv, exit
from tqdm import tqdm as bar
import keyboard
from threading import Thread

commands = ["git add *", "git commit -m 'Auto commit'", "git push -u"]
pause = float(argv[1]) * 60

print(pause)

sleeping = True

def run_commit(arg):
	global sleeping
	while True:
		print("Sleeping.")
		for i in bar(range(int(pause))):
			if sleeping:
				sleep(1)
			else:
				sleeping = True
				print("Committing now.")
				break

		# sleep(pause)

		print("Committing")
		# for c in commands:
		# 	print(c)
		# 	os.system(c)
		os.system("git add *")
		os.system("git commit -m 'Auto'")
		os.system("git push -u")

def check_press(arg):
	global sleeping
	while True:
		try:
			if keyboard.is_pressed('-'):
				sleeping = False
			else:
				pass
		except:
			break

		try:
			if keyboard.is_pressed('='):
				exit()
			else:
				pass
		except:
			break

if __name__ == "__main__":
	thread1 = Thread(target = run_commit, args = (0, ))
	thread2 = Thread(target = check_press, args = (0, ))
	print("Creating threads")
	thread1.start()
	thread2.start()
	thread1.join()
	thread2.join()
